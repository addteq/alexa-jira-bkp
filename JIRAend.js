'use strict';
var _ = require('lodash');
var rp = require('request-promise');

var CONFIG = require('./config.json');

// URL , Password and Username
var JIRAURL = CONFIG.JIRAServerURL;
var username = CONFIG.username;
var password = CONFIG.password;

// Authorization code and REST END Points
var Authorization = new Buffer(username + ':' + password).toString('base64');
var ENDPOINTURL = '/rest/api/latest';

function jiraEndHelper() {}

// Query JIRA using GET
jiraEndHelper.prototype.getJIRARESTCall = function(CALLENDPOINT) {
    var options = {
        method: 'GET',
        uri: JIRAURL + ENDPOINTURL + CALLENDPOINT,
        headers: {
            "Content-Type": "application/json",
            "Authorization": " Basic " + Authorization
        },
        json: true
    };
    return rp(options);
};

// POST call to JIRA to resolve issue
jiraEndHelper.prototype.postJIRAResolveIssue = function(jiraissue, CALLENDPOINT, TransitionId) {
    var postdata = {
        "transition": {
            "id": TransitionId
        }
    };

    var options = {
        method: 'POST',
        uri: JIRAURL + ENDPOINTURL + CALLENDPOINT,
        headers: {
            "Content-Type": "application/json",
            "Authorization": " Basic " + Authorization
        },
        body: postdata,
        json: true
    };
    return rp(options);
};

var getRandomString = function() {
    return new Array(5).join().replace(/(.|$)/g, function() {
        return ((Math.random() * 36) | 0).toString(36)[Math.random() < .5 ? "toString" : "toUpperCase"]();
    });
};

var getRandomIssueType = function() {
    return CONFIG.CreateIssueProperties.Issuetypes[Math.floor(Math.random() * 3)];
};

// POST JIRA create issue
jiraEndHelper.prototype.postJIRACreateIssue = function(CALLENDPOINT, projectkey) {
    var postdata = {
        "fields": {
            "project": {
                "key": projectkey.toUpperCase()
            },
            "summary": getRandomString(),
            "description": "Creating of an issue using project keys and issue type names using the REST API",
            "issuetype": {
                "name": getRandomIssueType()
            }
        }
    };

    var options = {
        method: 'POST',
        uri: JIRAURL + ENDPOINTURL + CALLENDPOINT,
        headers: {
            "Content-Type": "application/json",
            "Authorization": " Basic " + Authorization
        },
        body: postdata,
        json: true
    };
    return rp(options);
};

//  format last comment from jira issue
jiraEndHelper.prototype.formatJIRAIssueComment = function(issueInfo, jiraissue) {
    if (issueInfo.total) {
        var jiraSummary = _.template('The last comment on ${issue} is ${comment} .');

        var temp = jiraSummary({
            issue: jiraissue,
            comment: issueInfo.comments[issueInfo.total - 1].body
        });
        return temp;
    } else {
        return "Issue " + jiraissue + " does not have any comments";
    }
};

// Form Issue Query
jiraEndHelper.prototype.formatJIRAStatus = function(issueInfo) {
    var jiraSummary = _.template('${issue} ${summary} has a priority ${priority} and is in Status ${status}.');

    var temp = jiraSummary({
        issue: issueInfo.key,
        summary: issueInfo.fields.summary,
        priority: issueInfo.fields.priority.name,
        status: issueInfo.fields.status.name
    });
    return temp;
};

// Form Issue Assigned to me
jiraEndHelper.prototype.formatJIRAIssueAssignedToMe = function(issueInfo) {
    if (issueInfo.total === 0) {
        return 'There are no issue assigned to ' + CONFIG.username;
    } else {
        return 'Total issues assigned to ' + CONFIG.username + ' is ' + issueInfo.total;
    }
};

// Form Create JIRA ISsue
jiraEndHelper.prototype.formatCreateJIRAIssue = function(issueInfo) {
    return 'New JIRA issue ' + issueInfo.key + ' has been created  ';
};

// Form Resolve JIRA ISsue Response
jiraEndHelper.prototype.formatJIRAResolveIssue = function(issueInfo, jiraissue) {
    // Currently issueInfo is undefined so need to debug
    return jiraissue + ' has been resolved  to ' + CONFIG.ResolveJIRAIssue.ResolveState + ' status';
};

module.exports = jiraEndHelper;