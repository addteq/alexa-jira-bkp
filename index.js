module.change_code = 1;
var _ = require('lodash');
var Alexa = require('alexa-app');
var app = new Alexa.app('JIRAServer');
var jiraEndObject = require('./JIRAend');
var CONFIG = require('./config.json');

app.launch(function(req, res) {
    var prompt = 'Tell me the JIRA issue to get the status for';
    res.say(prompt).reprompt(prompt).shouldEndSession(false);
});

app.intent('StatusOfJiraIssue', {
        'slots': {
            'PROJECTKEY': 'PKEY',
            'ISSUENUMBER': 'NUMBER'
        },
        'utterances': ['{What is the Status|Status} {|of} {|Jira Issue} {-|PROJECTKEY} {-|ISSUENUMBER}']
    },
    function(req, res) {

        var projectkey = req.slot('PROJECTKEY');
        var issuenumber = req.slot('ISSUENUMBER');
        var reprompt = 'Tell me an JIRA issue to get the status.';

        if (_.isEmpty(projectkey) || _.isEmpty(issuenumber)) {
            var prompt = 'I didn\'t hear an JIRA issue. Tell me an JIRA issue to get the status.';
            res.say(prompt).reprompt(reprompt).shouldEndSession(false);
            return true;
        } else {
            var jiraHelper = new jiraEndObject();
            var jiraissue = projectkey + '-' + issuenumber;
            var ENDPOINT = '/issue/' + jiraissue;

            jiraHelper.getJIRARESTCall(ENDPOINT).then(function(responsebd) {
                res.say(jiraHelper.formatJIRAStatus(responsebd)).send();
            }).catch(function(err) {
                var prompt;
                if (err.statusCode === 404) {
                    prompt = 'Sorry , ' + jiraissue + ' does not exists on JIRA server';
                } else {
                    prompt = 'Sorry , I am not able to reach the JIRA server';
                }
                res.say(prompt).send();
            });
            return false;
        }
    }
);

app.intent('CommentOnJiraIssue', {
        'slots': {
            'PROJECTKEY': 'PKEY',
            'ISSUENUMBER': 'NUMBER'
        },
        'utterances': ['{|What is the} {last comment} {|on} {|Jira Issue} {-|PROJECTKEY} {-|ISSUENUMBER}']
    },
    function(req, res) {

        var projectkey = req.slot('PROJECTKEY');
        var issuenumber = req.slot('ISSUENUMBER');
        var reprompt = 'Tell me an JIRA issue to get the status.';

        if (_.isEmpty(projectkey) || _.isEmpty(issuenumber)) {
            var prompt = 'I didn\'t hear an JIRA issue. Tell me an JIRA issue to get the last comment from.';
            res.say(prompt).reprompt(reprompt).shouldEndSession(false);
            return true;
        } else {
            var jiraHelper = new jiraEndObject();
            var jiraissue = projectkey + '-' + issuenumber;
            var ENDPOINT = '/issue/' + jiraissue + '/comment';

            jiraHelper.getJIRARESTCall(ENDPOINT).then(function(responsebd) {
                res.say(jiraHelper.formatJIRAIssueComment(responsebd, jiraissue)).send();
            }).catch(function(err) {
                var prompt;
                if (err.statusCode === 404) {
                    prompt = 'Sorry , ' + jiraissue + ' does not exists on JIRA server';
                } else {
                    prompt = 'Sorry , I am not able to reach the JIRA server';
                }
                res.say(prompt).send();
            });
            return false;
        }
    }
);

app.intent('ResolveJiraIssue', {
        'slots': {
            'PROJECTKEY': 'PKEY',
            'ISSUENUMBER': 'NUMBER'
        },
        'utterances': ['{Resolve} {|Status} {|Jira Issue} {-|PROJECTKEY} {-|ISSUENUMBER}']
    },
    function(req, res) {
        var projectkey = req.slot('PROJECTKEY');
        var issuenumber = req.slot('ISSUENUMBER');
        var reprompt = 'Tell me an JIRA issue to resolve.';

        if (_.isEmpty(projectkey) || _.isEmpty(issuenumber)) {
            var prompt = 'I didn\'t hear an JIRA issue. Tell me an JIRA issue to resolve.';
            res.say(prompt).reprompt(reprompt).shouldEndSession(false);
            return true;
        } else {
            var jiraHelper = new jiraEndObject();
            var jiraissue = projectkey + '-' + issuenumber;
            var ENDPOINT = '/issue/' + jiraissue + '/transitions?expand=transitions.fields';
            var TransitionId;
            var TransitionState = CONFIG.ResolveJIRAIssue.ResolveState;

            jiraHelper.getJIRARESTCall(ENDPOINT).then(function(responsebd) {
                for (var key in responsebd.transitions) {
                    if (responsebd.transitions[key].name === TransitionState) {
                        TransitionId = responsebd.transitions[key].id;
                    }
                }

                if (typeof TransitionId !== 'undefined') {
                    jiraHelper.postJIRAResolveIssue(jiraissue, ENDPOINT, TransitionId).then(function(response) {
                        res.say(jiraHelper.formatJIRAResolveIssue(response, jiraissue)).send();
                    });
                } else {
                    res.say("Sorry , Unsupported Transition ,  There is no transition state " + TransitionState + " associated with this Project ").send();
                }
            }).catch(function(err) {
                var prompt;
                if (err.statusCode === 404) {
                    prompt = 'Sorry , ' + jiraissue + ' does not exists or you dont have permission to modify this issue';
                } else if (err.statusCode === 400) {
                    prompt = 'Sorry , The transitions specified for ' + jiraissue + ' does not exists or invalid';
                } else {
                    prompt = 'Sorry , I am not able to reach the JIRA server';
                }
                res.say(prompt).send();
            });
            return false;
        }
    }
);



app.intent('IssueAssigedToMe', {
        'utterances': ['{|What is the} {|number of} {|Jira} {Issues Assigned to me}']
    },
    function(req, res) {
        var jiraHelper = new jiraEndObject();
        var ENDPOINT = '/search?jql=assignee=' + CONFIG.username;

        jiraHelper.getJIRARESTCall(ENDPOINT).then(function(responsebd) {
            res.say(jiraHelper.formatJIRAIssueAssignedToMe(responsebd)).send();
        }).catch(function(err) {
            var prompt = 'Sorry , I am not able to reach the JIRA server';
            res.say(prompt).send();
        });
        return false;
    }
);

app.intent('CreateJIRAIssue', {
        'slots': {
            'PROJECTKEY': 'PKEY'
        },
        'utterances': [' {Create} {|JIRA} {|Issue} {|in} {-|PROJECTKEY}']
    },
    function(req, res) {
        var projectkey = req.slot('PROJECTKEY');
        var reprompt = 'Do you want to create a JIRA issue with loren ipsum data in DEMO Project ?.';

        if (_.isEmpty(projectkey)) {
            var prompt = 'I didn\'t hear a JIRA Project to Create the Issue In . Default project that could be used is ' + CONFIG.CreateIssueProperties.ProjectKey;
            res.say(prompt).reprompt(reprompt).shouldEndSession(false);
            return true;
        } else {

            var jiraHelper = new jiraEndObject();
            var ENDPOINT = '/issue';

            jiraHelper.postJIRACreateIssue(ENDPOINT, projectkey).then(function(responsebd) {
                res.say(jiraHelper.formatCreateJIRAIssue(responsebd)).send();
            }).catch(function(err) {
                var prompt;
                if (err.statusCode === 400) {
                    prompt = 'Invalid project Key. Please provide a Valid project key to create the issue in';
                } else {
                    prompt = 'Sorry , I am not able to reach the JIRA server';
                }
                res.say(prompt).send();
            });
            return false;
        }
    }
);

//hack to support custom utterances in utterance expansion string
console.log(app.utterances().replace(/\{\-\|/g, '{'));
module.exports = app;
